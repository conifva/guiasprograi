import gi
gi.require_version("Gtk", "3.0")

from gi.repository import Gtk


class ventana:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("ventana_princ.glade")
        self.window = builder.get_object("window")

        self.texto1 = builder.get_object("entry2")
        self.texto2 = builder.get_object("entry1")

        # hacer que se calcule el numero al apretar enter
        # es "activate" según https://lazka.github.io/pgi-docs/#Gtk-3.0/classes/Entry.html#signals
        self.texto1.connect("activate", self.calc)
        self.texto2.connect("activate", self.calc)

        self.boton_aceptar = builder.get_object("button1")
        self.boton_reset = builder.get_object("reset")

        self.boton_aceptar.connect("clicked", self.info)
        self.boton_reset.connect("clicked", self.confirmar_reset)

        self.totalcaracteres = builder.get_object("spinbutton1")

        self.window.set_default_size(800, 600)
        self.window.connect("destroy", Gtk.main_quit)
        self.window.show_all()

    def reset(self, button=None):
        self.texto1.set_text("")
        self.texto2.set_text("")
        self.totalcaracteres.set_value(0)

    def calc(self, button=None):
        largo1 = len(self.texto1.get_text())
        largo2 = len(self.texto2.get_text())
        self.totalcaracteres.set_value(largo1 + largo2)

    def info(self, button=None):
        text1 = self.texto1.get_text()
        text2 = self.texto2.get_text()
        largo = len(text1) + len(text2)

        tipo_info = Gtk.MessageType.INFO
        botones = Gtk.ButtonsType.YES_NO

        dialog = Gtk.MessageDialog(type=tipo_info, buttons=botones)
        dialog.format_secondary_text("Texto 1: " + text1 + "\nTexto 2: " + text2 + "\nLargo de ambas cadenas: " + str(largo) + "\n\nReiniciar la suma a cero y borrar las entradas de texto?")
        dialog.connect("response", self.respuesta_dialogo)
        dialog.run()

    def respuesta_dialogo(self, dialogo, respuesta):
        if respuesta == Gtk.ResponseType.YES:
            self.reset()

        dialogo.close()

    def confirmar_reset(self, button=None):
        # https://lazka.github.io/pgi-docs/#Gtk-3.0/enums.html#Gtk.MessageType.INFO
        tipo_info = Gtk.MessageType.INFO
        # https://lazka.github.io/pgi-docs/#Gtk-3.0/enums.html#Gtk.ButtonsType.YES_NO
        botones = Gtk.ButtonsType.YES_NO

        # abre un dialogo de información con botones SI y NO
        dialogo = Gtk.MessageDialog(type=tipo_info, buttons=botones)
        dialogo.format_secondary_text("Realmente reiniciar las entradas de texto?")
        dialogo.connect("response", self.respuesta_dialogo)
        dialogo.run()


if __name__ == "__main__":
    w = ventana()
    Gtk.main()