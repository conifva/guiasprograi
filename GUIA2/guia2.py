from database import Database
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class window():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana_princ.glade")
        self.window = self.builder.get_object("ventana_princ")
        self.window.show_all()
        #self.window.maximize()
        self.window.connect("destroy", Gtk.main_quit)
        self.btn_editar = self.builder.get_object("btn_editar")
        self.btn_editar.connect("clicked", self.editar_selec)
        self.btn_agregar = self.builder.get_object("btn_agregar")
        self.btn_agregar.connect("clicked", self.agregar_compuesto)
        self.btn_borrar = self.builder.get_object("btn_borrar")
        self.btn_borrar.connect("clicked", self.borrar_selec)
        

        self.listmodel = Gtk.ListStore(str, str, str)
        self.table = self.builder.get_object("treeview")
        self.table.set_model(model=self.listmodel)

        cell = Gtk.CellRendererText()
        titles = ["Nombre", "Categoria", "Composicion Quimica"]
        for i in range(len(titles)):
            col = Gtk.TreeViewColumn(titles[i], cell, text=i)
            self.table.append_column(col)

        self.lista()

    def selec(self):
        model, it = self.table.get_selection().get_selected()
        if it is None:
            return
        Nombre = model.get_value(it, 0)
        Categoria = model.get_value(it, 1)
        Composicion = model.get_value(it, 2)

        return (Nombre, Categoria, Composicion)

    def lista (self):
        if len(self.listmodel) > 0:
            for i in range(len(self.listmodel)):
                iter = self.listmodel.get_iter(0)
                self.listmodel.remove(iter)

        data = Database.load("diccionario.json")
        for key in list(data.keys()):
            for item in data[key]:
                self.listmodel.append([item[0], key, item[1]])

    def agregar_compuesto(self, boton=None):
        def actualizar(boton=None):
            self.lista()

        window = agregar()
        window.window.connect("destroy", actualizar)

    def borrar_selec(self, boton=None):
        Nombre, Categoria, Composicion = self.selec()

        if Nombre == "":
            return

        mensaje = Gtk.MessageDialog(type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.YES_NO)
        mensaje.format_secondary_text("Desea borrar " + Nombre + " ?")

        def response(dialogo, seleccionado):
            dialogo.close()
            si = Gtk.ResponseType.YES

            if seleccionado == si:
                self.borrar_confirmacion(Nombre, Categoria, Composicion)
                self.lista()

        mensaje.connect("response", response)
        mensaje.run()


    def borrar_confirmacion(self, Nombre, Categoria, Composicion):
        data = Database.load("diccionario.json")

        for i in range(len(data[Categoria])):
            if data[Categoria][i][0] == Nombre and data[Categoria][i][1] == Composicion:
                del data[Categoria][i]
                break

        Database.save("diccionario.json", data)

    def editar_selec(self, boton=None):
        return


class agregar:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("agregar.glade")
        self.window = self.builder.get_object("ventana-agrega")
        self.window.show_all()
        self.Nombre = self.builder.get_object("Nombre")
        self.Composicion = self.builder.get_object("Composicion")
        self.combobox = self.builder.get_object("combobox")
        self.btn_Cancelar = self.builder.get_object("Cancelar")
        self.btn_Aceptar = self.builder.get_object("Aceptar")
        self.btn_Cancelar.connect("clicked", self.salir)
        self.btn_Aceptar.connect("clicked", self.agregar)


    def salir(self, boton=None):
        self.window.close()


    def agregar(self, boton=None):
        Nombre = self.Nombre.get_text()
        Categoria = self.combobox.get_model()[self.combobox.get_active()][0]
        Composicion = self.Composicion.get_text()

        if Composicion == "" or self.combobox.get_active() == -1 or Nombre == "":
            return

        elementos = Database.load("diccionario.json")
        elementos[Categoria].append([Nombre, Composicion])
        Database.save("diccionario.json", elementos)
        self.window.close()

if __name__ == "__main__":
    window = window()
    Gtk.main()
